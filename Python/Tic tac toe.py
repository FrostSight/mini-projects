import random

#variable initialization
board = ["-", "-", "-",
        "-", "-", "-",
        "-", "-", "-",]
currentPlayer = "X"
gameRunning = True
winner = None

def playingBoard(board):
    print (board[0] + " | " + board[1] + " | " + board[2])
    print("-----------")
    print (board[3] + " | " + board[4] + " | " + board[5])
    print("-----------")
    print (board[6] + " | " + board[7] + " | " + board[8])

def playerInput(board):
    input1 = int(input("\nEnter any number from 1 to 9: "))
    
    if input1 >= 1 and input1 <= 9 and board[input1 - 1] == "-":
        board[input1 - 1] = currentPlayer
    else:
        print("Invalid input")

#*Game results
def horizontyalWin(board):
    global winner
    if board[0] == board [1] == board[2] and board[1] != "-":
        winner = board[0]
        return True
    elif board[3] == board [4] == board[5] and board[3] != "-":
        winner = board[3]
        return True
    elif board[6] == board [7] == board[8] and board[6] != "-":
        winner = board[6]
        return True

def verticalWin(board):
    global winner
    if board[0] == board [3] == board[6] and board[0] != "-":
        winner = board[0]
        return True
    elif board[1] == board [4] == board[7] and board[1] != "-":
        winner = board[1]
        return True
    elif board[2] == board [5] == board[8] and board[2] != "-":
        winner = board[2]
        return True

def daiagonalWin(board):
    global winner
    if board[0] == board[4] == board[8] and board[0] != "-":
        winner = board[0]
        return True
    elif board[2] == board[4] == board[6] and board[2] != "-":
        winner = board[2]
        return True

def checkTie(board):
    global gameRunning
    if "-" not in board:
        gameRunning = False
        playingBoard(board)
        print("This game is a tie!")

def checkWin():
    global gameRunning
    if horizontyalWin(board) or verticalWin(board) or daiagonalWin(board):
        gameRunning = False
        print(f"The winner is : {winner}")

#?Player switching
def switchPlayer():
    global currentPlayer
    if currentPlayer == "X":
        currentPlayer = "0"
    else:
        currentPlayer = "X"

def player2(board):
    while currentPlayer == "0":
        position = random.randint(0, 8)
        if board[position] == "-":
            board[position] = "0"
            switchPlayer()

while gameRunning:
    playerInput(board)
    checkWin()
    checkTie(board)
    switchPlayer()
    player2(board)
    playingBoard(board)
    checkWin()
    checkTie(board)