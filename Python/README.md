
# Tic Tac Toe in Python

This is a simple Tic Tac Toe game implemented in Python. You can play against a random AI opponent.

## Documentation

**How to Play:**
- Clone this repository.
- Open a terminal or command prompt and navigate to the directory containing the script (tic_tac_toe.py).
- Run the script using python tic_tac_toe.py.
- Follow the on-screen instructions to make your moves by entering a number between 1 and 9 corresponding to the position on the board.

**Features:**
- Play against a random AI opponent.
- Checks for horizontal, vertical, and diagonal wins.
- Announces the winner or tie at the end of the game.

**Dependencies:**
This code requires no external libraries beyond the standard Python library.
## Tech used

**Language:** Python

**Tool:** Visual Studio Code

## Screenshots

![Screenshot_2023-03-04_203654](/uploads/7261f01e7e2ad38dffef8c7e5072322a/Screenshot_2023-03-04_203654.png)


## Feedback

If you have any feedback, please reach out to at http://scr.im/84ta

