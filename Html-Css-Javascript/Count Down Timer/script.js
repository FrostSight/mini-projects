const days = document.getElementById('days');
const hours = document.getElementById('hours');
const mins = document.getElementById('mins');
const secs = document.getElementById('secs');

const formateTime = (time) => {
    return time < 10 ? `0${time}` : time;
}

const updateCountDown = (deadline) => {
    const currentTime = new Date();
    const timeDifference = deadline - currentTime;

    let calSecs = Math.floor(timeDifference / 1000) % 60;
    let calMins = Math.floor(timeDifference / 1000 / 60) % 60;
    let calHours = Math.floor(timeDifference / 1000 / 60 / 60) % 24;
    let calDays = Math.floor(timeDifference / 1000 / 60 / 60 / 24);

    days.textContent = formateTime(calDays);
    hours.textContent = formateTime(calHours);
    mins.textContent = formateTime(calMins);
    secs.textContent = formateTime(calSecs);
}

const countDown = (targetDate) => {
    setInterval(() => updateCountDown(targetDate), 1000);
}

// const targetDate = new Date("April 01 2024 12:00");
// countDown(targetDate);
const targetDate = new Date();
targetDate.setDate(targetDate.getDate() + 1);  // always picking tomorrow's date
countDown(targetDate);