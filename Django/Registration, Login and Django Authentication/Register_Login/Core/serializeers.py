from rest_framework import serializers
from .models import *

class StudentSerializer(serializers.ModelSerializer):  # student model er serializer banaisi
    class Meta:
        model = Student
        fields  = '__all__'


class SignUpSerializer(serializers.Serializer): # model charai serializer banaisi
    username = serializers.CharField(max_length=20)
    password = serializers.CharField(max_length=20)