from django.shortcuts import render
from .serializeers import *
from .models import *
from rest_framework.views import APIView
from rest_framework.response import Response
from django.contrib.auth import authenticate
from rest_framework.authtoken.models import Token
from rest_framework.permissions import IsAuthenticated

# Create your views here.

class StudentAPI(APIView):
    permission_classes = [IsAuthenticated]
    def get(self, request):
        print(request.user)
        students = Student.objects.all()
        serial_izers = StudentSerializer(students, many=True)
        return Response({
            "Status" : True,
            "Data" : serial_izers.data
        }) 

class SignUpAPI(APIView):
    def post(self, request):
        r_data = request.data
        serial_izer = SignUpSerializer(data=r_data)
        if not serial_izer.is_valid():
            return Response({
                'Status' : False,
                "data" : serial_izer.errors
            })
        
        username_ = serial_izer.data["username"]  # using kind of different variables to track keywords
        password_ = serial_izer.data["password"]
        print(username_ + "\n" + password_)

        # authenticate
        user_obj = authenticate(user_name=username_, pass_word=password_)
        if user_obj:       # authenticated (may be field k authenticate kore)
            token, _ = Token.objects.get_or_create(user=user_obj)
            print(token.token)
            return Response({
                "Status" : True,
                "Data" : {"token" : str(token)},
            })

        print(r_data)
        return Response({  # not authencated
            "Status" : True,
            "Data" : {},
            "message" : "Invalid credentials"
        })