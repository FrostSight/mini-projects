from django.db import models

# Create your models here.
class Student(models.Model):
    name = models.CharField(max_length=20)
    age = models.IntegerField(default=10)
    notes = models.TextField(blank=True, max_length=100)
    
    def __str__(self) -> str:
        return f"{self.id} : {self.name}"