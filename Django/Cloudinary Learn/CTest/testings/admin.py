from django.contrib import admin
from .models import *

# Register your models here.

class EmployeeImageInline(admin.StackedInline):
    model = EmployeeImage
    extra = 1


class EmployeeAdmin(admin.ModelAdmin):
    model = Employee
    list_display = ('id', 'name')
    ordering = ('id',)
    
    inlines = [EmployeeImageInline]

admin.site.register(Employee, EmployeeAdmin)