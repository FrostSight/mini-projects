from django.db import models
from cloudinary.models import CloudinaryField

class Employee(models.Model):
    name = models.CharField(max_length=100)
    
    def __str__(self):
        return f"ID {self.id}: {self.name}"

class EmployeeImage(models.Model):
    employee = models.ForeignKey(Employee, on_delete=models.CASCADE)
    image = CloudinaryField('image')
    
    def __str__(self):
        return f"ID {self.id}: {self.employee.name}"
