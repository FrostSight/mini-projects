from rest_framework import serializers
from .models import Employee, EmployeeImage

class EmployeeImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = EmployeeImage
        fields = ['id', 'employee', 'image']

class EmployeeSerializer(serializers.ModelSerializer):
    images = EmployeeImageSerializer(many=True, read_only=True)
    
    class Meta:
        model = Employee
        fields = ['id', 'name', 'images']
